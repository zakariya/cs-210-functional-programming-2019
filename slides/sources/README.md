# Lecture Slides for the EPFL/Coursera course _Functional Programming Principles in Scala_

This repository contains the source for EPFL and Coursera course on functional programming in Scala.

From markdown source, it generates PDF slides based on the [LaTeX beamer class](https://bitbucket.org/rivanvx/beamer/wiki/Home).

## Dependencies

1. A standard TeX Live distribution.
  - For OS X there's [MacTeX](http://tug.org/mactex/)
  - For Linux and Windows, there's [TeX Live](http://www.tug.org/texlive/)
2. [Pandoc](http://johnmacfarlane.net/pandoc/), a universal document translator
  - Installers for [OSX and Windows](http://code.google.com/p/pandoc/downloads/list)
  - For Linux, [try your standard package manager](http://johnmacfarlane.net/pandoc/installing.html)
3. [Pygments](http://pygments.org/) an automatic syntax highligher
  - Given that Python is installed on your system, to install, simply run:

    `sudo easy_install Pygments`
(If you already have pygments and you get any latex errors that say anything about `out.pyg` files, just run the above to update pygments)
4. We also use Eclise-themed syntax-highlighting. One-time install, from the root directory, run:

    `cd _resources/syntax-highlighting/ ; sudo python setup.py install ; cd ../..`

It also assumes that you have Scala installed, reachable using a `scala` alias.

**Note:** Ubuntu/Debian users should install python-setuptools if it's not already installed.

**Note:** You should have the Inconsolata font installed.

## Running

Make sure you're in the repo's root directory. Then, simply run:

    ./slidegen FILENAME

Please note that `FILENAME` that you pass to `slidegen` must not have any extension (though, of course, it should be markdown source file). The generated PDF slides are available in the `gen` directory, and the intermediate generated *.tex files are visible in `gen/tex`.

Source files are assumed to live in the root directory of the repository.

Example usage, given a file `lecture1.md` in a root directory named `progfun-slides`:

    ./slidegen lecture1
    
To have the title slide generated for the "Programming Reactive Systems" course, run with the `-Vreactive` option:

    ./slidegen lectureX -V reactive=true


### Running remotely

If you are having trouble resolving all the dependencies, run the `remotegen` script from the `remotegen` directory.
It will copy the respective markdown file to `chara`, build it there, and copy it back.

    remotegen/remotegen week1-1 <extra-options>


## Usage

Standard markdown may be used without any syntactical changes. The convention is that the largest headline (prefixed by `#` or underlined via `==`) denotes the beginning of a new slide. For example:

    # My Title!

    - a
    - bulleted
    - list

is equivalent to:

    My Title!
    =========

    - a
    - bulleted
    - list

### Code

As in standard markdown, multi-line code blocks are denoted by a prefix of 4 empty spaces on each line of code, for example:

    ## Markdown "h2" title

        def foo(f: Foo): Bar = {
          ...
        }

Inline code is denoted by simple backticks, for example:

    This is an inline sentence, with import statement, `import scala.sys.process._`

### Math

Latex math mode is supported via `$$` or `\begin{equation}\end{equation}`. Since the markdown document is first translated into a latex beamer document, and then translated to a PDF, latex can be used throughout the document (for example, in the case where you want custom control over a the layout of a figure.)

### Images

Images can be included using standard markdown markup. Just make sure you place the images in the `images` folder located in the root directory. Example usage:

    ![pic1](images/PIC1.jpeg)

As alluded to in the previous section, for more control, you can escape to standard latex. For example:

    \centerline{\includegraphics[height=2in]{images/PIC1.jpeg}}

## Markdown

A quick overview of Markdown syntax:

![mou app](http://mouapp.com/images/Mou_Screenshot_1.png)

You can also work within a markdown editor, such as [Mou](http://mouapp.com/) (shown above), or [Marked](http://markedapp.com/) (Both for OS X only).
